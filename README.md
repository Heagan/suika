# Suika Game Godot

[Play in your browser](https://suika-heagan-d8c2abc16cf303726783c714864c6100b2a2fe258ac81fcf81.gitlab.io/)
<br>
Host Yourself: [Prebuilt Web Build](https://gitlab.com/Heagan/suika/-/releases/)
<br>
<br>
This project is a remake of the game Suika.
<br> 
It was made for fun as it is a simple game and a good way to learn Godot
<br> 
<br> 
Requires Godot 4.2.1+
![](suika.png)

## Build and Serving
Designed with the addon Box2D for the physics, without it the fruit bounce too high and have bad physics

Designed to run in the browser.
<br> 
To serve run:
<br> 
`npm i serve`
<br> 
`npx serve --cors`
<br> 
### Fix headers error
<br> 
https://github.com/gzuidhof/coi-serviceworker
<br> 
Project needs to run on HTTPS

