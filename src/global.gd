extends Node2D

const fruits = [
	preload("res://assests/cherry.png"),
	preload("res://assests/strawberry.png"),
	preload("res://assests/grape.png"),
	preload("res://assests/apricot.png"),
	preload("res://assests/persimmon.png"),
	preload("res://assests/apple.png"),
	preload("res://assests/pear.png"),
	preload("res://assests/peach.png"),
	preload("res://assests/pineapple.png"),
	preload("res://assests/melon.png"),
	preload("res://assests/watermelon.png"),
]

var score: int = 0
var best_score: int = 0

var next_fruit_index: int = 0
var current_fruit_index: int = 0

var SCALE_MIN: float = 0.20
var SCALE_MAX: float = 1.4

var GAME_OVER: bool = false

var combine: AudioStream = preload("res://assests/audio/pop.ogg")
var lose: AudioStream = preload("res://assests/audio/slash.ogg")

var destroy_fruit: bool = false

func _stop_after_duration(player: Node):
	if not player:
		return
	player.stop()
	player.queue_free()

func play_sound(parent: Node, stream: AudioStream, duration: float = -1):
	if duration == -1:
		duration = stream.get_length()
	var timer = parent.get_tree().create_timer(duration)
	var audio_stream_player = AudioStreamPlayer.new()
	audio_stream_player.stream = stream
	parent.add_child(audio_stream_player)
	audio_stream_player.play()
	timer.timeout.connect(_stop_after_duration.bind(audio_stream_player))
