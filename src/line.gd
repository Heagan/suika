extends Sprite2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var left_wall = get_parent().get_node("walls/left_wall/CollisionShape2D")
	var right_wall = get_parent().get_node("walls/right_wall/CollisionShape2D")
	position.x = clamp(get_global_mouse_position().x, left_wall.global_position.x + 50, right_wall.global_position.x - 50)
