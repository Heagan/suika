extends Area2D

var index: int = -1

@onready
var sprite = $spawner_coll/spawner_sprite

func _ready():
	connect("body_entered", _on_body_entered)


func change_icon():
	var chosen_fruit = Globals.fruits[Globals.current_fruit_index]
	var n_scale = remap(Globals.current_fruit_index, 0, 10, Globals.SCALE_MIN, Globals.SCALE_MAX)

	sprite.texture = chosen_fruit
	scale = Vector2(n_scale, n_scale)


func _process(_delta):
	var left_wall = get_parent().get_node("walls/left_wall/CollisionShape2D")
	var right_wall = get_parent().get_node("walls/right_wall/CollisionShape2D")
	if not left_wall or not right_wall:
		return
	var position_x = clamp(get_global_mouse_position().x, left_wall.global_position.x + 50, right_wall.global_position.x - 50)
	set_position(Vector2(position_x, 175))
	if Globals.current_fruit_index != index:
		index = Globals.current_fruit_index
		change_icon()


func _on_body_entered(body):
	if body.enabled:
		Globals.GAME_OVER = true
