extends Node

@export var shiver_amount: float = 2.0  # Control the intensity of the shiver
@export var update_interval: float = 0.1  # Time in seconds between updates
var initial_positions = []  # Array to store initial positions
var next_positions = []  # Array to store next target positions for interpolation
var timer = Timer.new()  # Timer for controlling update frequency


func _ready():
	for child in get_children():
		if child is Sprite2D:
			initial_positions.append(child.position)
			next_positions.append(child.position)
	add_child(timer)
	timer.wait_time = update_interval
	timer.autostart = true
	timer.connect("timeout", _on_Timer_timeout)
	timer.start()

func _on_Timer_timeout():
	var index = 0
	for child in get_children():
		if child is Sprite2D:
			var base_position = initial_positions[index]
			var offset_x = randf_range(-shiver_amount, shiver_amount)
			var offset_y = randf_range(-shiver_amount, shiver_amount)
			next_positions[index] = base_position + Vector2(offset_x, offset_y)
			index += 1


func _process(delta: float):
	var index = 0
	for child in get_children():
		if child is Sprite2D:
			child.position = child.position.lerp(next_positions[index], delta * 5)  # Adjust the factor to control speed of interpolation
			index += 1

		#var base_position = initial_positions[index]
		#var offset_x = randf_range(-shiver_amount, shiver_amount)
		#var offset_y = randf_range(-shiver_amount, shiver_amount)
		#child.position = base_position + Vector2(offset_x, offset_y)
	
