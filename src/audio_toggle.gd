extends Area2D

@export var mode = 0

@onready
var bg_audio: AudioStreamPlayer = $MainSong


func audio_off():
	bg_audio.stream.loop = true
	bg_audio.volume_db = -100
	$FruitDrop.volume_db = -100
	$FruitDrop2.volume_db = -100
	$Loose.volume_db = -100
	$AudioOn.visible = false
	$AudioHalf.visible = false
	$AudioOff.visible = true

func audio_half():
	bg_audio.volume_db = -15
	$FruitDrop.volume_db = -25
	$FruitDrop2.volume_db = -25
	$Loose.volume_db = -25
	bg_audio.stream.loop = true
	$AudioOn.visible = false
	$AudioHalf.visible = true
	$AudioOff.visible = false

func audio_full():
	bg_audio.volume_db = 0
	$FruitDrop.volume_db = 0
	$FruitDrop2.volume_db = 0
	$Loose.volume_db = 0
	bg_audio.stream.loop = true
	$AudioOn.visible = true
	$AudioHalf.visible = false
	$AudioOff.visible = false

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
		mode += 1
		if mode == 0:
			audio_full()
		elif mode == 1:
			audio_half()
		elif mode == 2:
			audio_off()
		if mode == 2: mode = -1

func _ready():
	bg_audio.stream.loop = true
	bg_audio.volume_db = 0
