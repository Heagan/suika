extends Node2D


func get_random_index():
	var rand_pick = randf()
	if rand_pick < 0.35: return 0
	if rand_pick < 0.65: return 1
	if rand_pick < 0.8: return 2
	if rand_pick < 0.90: return 3
	return 4


func change_preview():
	"""Checked Approved"""
	var fruit_index = get_random_index()
	var chosen_fruit = Globals.fruits[fruit_index]

	var preview_sprite: Sprite2D = get_node("preview_body/preview_collision/preview_sprite")
	var preview_scale = remap(fruit_index, 0, Globals.fruits.size() - 1, 0.15, 0.85) * 5

	preview_sprite.texture = chosen_fruit
	preview_sprite.scale = Vector2(preview_scale, preview_scale)

	Globals.next_fruit_index = fruit_index 


func spawn_fruit(index: int = -1, spawn_location: Vector2 = Vector2(250, 450), is_extra: bool = false):
	if index == -1:
		$audio_toggle/FruitDrop.playing = true
		index = Globals.current_fruit_index
		Globals.current_fruit_index = Globals.next_fruit_index
	else:
		$audio_toggle/FruitDrop2.playing = true
		#if not is_extra:
			#const fruit_mini = preload("res://src/fruit.tscn")
			#var fruit_instance_mini = fruit_mini.instantiate()
			#fruit_instance_mini.is_extra = true
			#fruit_instance_mini.fruit_type = index
			#var chosen_fruit_mini = Globals.fruits[index]
			#var fruit_sprite_mini: Sprite2D = fruit_instance_mini.get_node("collision/sprite")
			#fruit_sprite_mini.texture = chosen_fruit_mini
			#add_child(fruit_instance_mini)

	const fruit = preload("res://src/fruit.tscn")
	var fruit_instance = fruit.instantiate()
	fruit_instance.is_extra = is_extra
	fruit_instance.spawn_location = spawn_location
	fruit_instance.fruit_type = index 
	var chosen_fruit = Globals.fruits[index]
	var fruit_sprite: Sprite2D = fruit_instance.get_node("collision/sprite")
	fruit_sprite.texture = chosen_fruit
	if index == -1: fruit_instance.enabled = true

	add_child(fruit_instance)
	fruit_instance.connect("fruit_combine_event", spawn_fruit)
	return fruit_instance


func _ready():
	change_preview()


func _input( event ):
	if Input.is_key_pressed(KEY_ESCAPE):
		get_tree().quit()
	if event is InputEventMouseButton and not Globals.destroy_fruit:
		if not event.pressed and event.button_index == MOUSE_BUTTON_LEFT:
			if not Globals.GAME_OVER:
				var left_wall = get_node("walls/left_wall/CollisionShape2D")
				var right_wall = get_node("walls/right_wall/CollisionShape2D")
				var mx = clamp(get_global_mouse_position().x, left_wall.global_position.x + 50, right_wall.global_position.x - 50)
				if abs(get_global_mouse_position().x - mx) < 150:
					spawn_fruit(-1, Vector2(mx, 200), false)
					change_preview()

var doOnce = true
func _process(_delta):
	$score_body/score.text = str(Globals.score)
	$track.visible = Globals.GAME_OVER
	if Globals.GAME_OVER:
		doOnce = false
		$track.text = "Game Over!\nScore: " + str(Globals.score)
		if Globals.score > Globals.best_score:
			Globals.best_score = Globals.score
			$score_body/score/best_score.text = str(Globals.best_score)
		$track.visible = true
		$audio_toggle/Loose.playing = true

func restart_game():
	if Globals.score > Globals.best_score:
		Globals.best_score = Globals.score
	Globals.score = 0
	Globals.next_fruit_index = get_random_index()
	Globals.current_fruit_index = 0
	for child in get_children():
		if child is RigidBody2D:
			child.queue_free()
	Globals.GAME_OVER = false
	$score_body/score/best_score.text = str(Globals.best_score)


func _on_button_button_down():
	restart_game()
