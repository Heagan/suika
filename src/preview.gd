extends CharacterBody2D

var speed = 100  # Adjust this value to control the overall movement speed
var amplitude = 5  # Adjust this value to control the height of the movement
var frequency = 2  # Adjust this value to control the frequency of the movement (higher = faster oscillation)

var time_elapsed = 0.0  # Track elapsed time


func _physics_process(delta):
	# Get the sine value for the current oscillation phase
	time_elapsed += delta  # Update elapsed time
	var offset = sin(time_elapsed * frequency) * amplitude

	# Apply the offset to the y-axis movement
	velocity = Vector2.UP * offset * delta * speed
	move_and_slide()
