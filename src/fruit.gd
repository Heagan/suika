extends RigidBody2D

signal fruit_combine_event

var fruit_type: int = 1
var enabled: bool = false
var max_velocity = 800
var is_extra: bool = false
var spawn_location: Vector2
var already_collided: bool = false  # sometimes a fruit touches 2 obj at the same time, only 1 should disappear 


func _ready():
	set_position(spawn_location)

	var timer = $Timer
	timer.connect("timeout", _on_Timer_timeout)
	timer.start()
	if is_extra:
		var kill_timer = $KillTimer
		kill_timer.connect("timeout", queue_free)
		kill_timer.start()
	
func _on_Timer_timeout():
	enabled = true


func _input( event ):
	if event is InputEventMouseButton and (Input.is_mouse_button_pressed(MOUSE_BUTTON_RIGHT) or Globals.destroy_fruit):
		if not Globals.GAME_OVER:
			if self.position.distance_to(get_global_mouse_position()) < self.scale.length() * 30:
				#Globals.destroy_fruit = false
				Globals.score = max(Globals.score - (fruit_type * 2) * 100, 0)
				Globals.play_sound(get_parent(), Globals.lose)
				self.queue_free()


func _physics_process(delta):
	var n_scale = remap(fruit_type, 0, 10, Globals.SCALE_MIN, Globals.SCALE_MAX)
	if is_extra:
		n_scale = Globals.SCALE_MIN
	#if fall:
		#gravity_scale = 0.5
	get_node("collision").scale = Vector2(n_scale, n_scale)

	if linear_velocity.length() > 50:
		apply_torque(linear_velocity.angle() + (90 if randi_range(0, 1) else -90))


func _on_body_entered(body):
	if not enabled or already_collided: return

	var other_node = body
	if other_node is RigidBody2D:
		if other_node.get_node("collision/sprite").texture == self.get_node("collision/sprite").texture:
			if not is_extra:
				Globals.score += (fruit_type * 2) * 10
			emit_signal("fruit_combine_event", fruit_type + 1, (self.position + other_node.position) / 2, is_extra)
			if not is_extra:
				emit_signal("fruit_combine_event", fruit_type + 1, Vector2(randi_range(100, 450), 500), true)
			already_collided = true
			other_node.enabled = false
			other_node.queue_free()
			self.queue_free()
	if other_node is StaticBody2D and position.y < 150:
		Globals.play_sound(get_parent(), Globals.lose)
		Globals.GAME_OVER = true
