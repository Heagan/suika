extends Sprite2D


func _process(_delta):
	var left_wall = get_parent().get_node("walls/left_wall/CollisionShape2D")
	var right_wall = get_parent().get_node("walls/right_wall/CollisionShape2D")
	position.x = 60 + clamp(get_global_mouse_position().x, left_wall.global_position.x + 50, right_wall.global_position.x - 50)
