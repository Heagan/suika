extends Area2D


func _process(delta):
	if Globals.destroy_fruit:
		$Explode.set_modulate(Color(1, 0.9, 0.5, 1))
	else:
		$Explode.set_modulate(Color(1, 1, 1, 1))

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
		Globals.destroy_fruit = !Globals.destroy_fruit
